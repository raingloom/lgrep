#!/bin/env lua
local pattern
local captures
local nobuf
local files = {}
local printlineno = false
local printfilename = false
local whole = false
local use_gmatch = false

local usage = arg[0] .. [=[
 [-e expr] [-c c1,c2,...] [-n] [-f] [-b] [-W] [-g] [--] files...]=]

local function abort_usage()
	io.stderr:write(usage)
	os.exit(false,false)
end

while #arg > 0 do
	local a = table.remove(arg, 1)
	if a == '-e' then
		pattern = table.remove(arg, 1) or abort_usage()
	elseif a == '-c' then
		local i = 1
		captures = {}
		for c in (table.remove(arg, 1) or abort_usage()):gmatch('%d+') do
			captures[i], i = tonumber(c), i+1
		end
	elseif a == '-n' then
		printlineno = true
	elseif a == '-b' then
		nobuf = true
	elseif a == '-W' then
		--WARNING: this won't work with infinite streams!!!
		whole = true
	elseif a == '-g' then
		use_gmatch = true
	elseif a == '-f' then
		printfilename = true
	else
		if a ~= '--' then
			table.insert(files, a)
		end
		for i = 1, #arg do
			table.insert(files, arg[i])
			arg[i] = nil
		end
		break
	end
end

if pattern then
	--test pattern
	string.match('', pattern)
else
	abort_usage()
end

local function iter_file(file)
	if whole then
		return coroutine.wrap(function()
			coroutine.yield(file:read('*a'))
		end)
	else
		return file:lines()
	end
end

local function iter_matches(str, pattern)
	if use_gmatch then
		return coroutine.wrap(function()
			local gen = str:gmatch(pattern)
			local matches
			while true do
				matches = table.pack(gen())
				if matches[1] then
					coroutine.yield(matches)
				else
					return
				end
			end
		end)
	else
		return coroutine.wrap(function()
			coroutine.yield(table.pack(str:match(pattern)))
		end)
	end
end

local function process(file, path)
	local lineno = 0
	for line in iter_file(file) do
		lineno = lineno + 1
		for matches in iter_matches(line, pattern) do
			--don't use .n because table.pack(nil).n==1
			if #matches > 0 then
				if printfilename then
					io.write(path,':')
				end
				if printlineno then--not very meaningful with -W but let's allow it for now
					io.write(lineno,':')
				end
				if captures then
					for i = 1, #captures do
						io.write(matches[captures[i]] or '') --FIXME: should this error?
					end
				else
					io.write(table.concat(matches))
				end
				io.write('\n')
				if nobuf then
					io.stdout:flush()
				end
			end
		end
	end
end

if #files>0 then
	for _, path in ipairs(files) do
		local file = assert(io.open(path,'r'))
		process(file, path)
	end
else
	process(io.stdin, '')
end
